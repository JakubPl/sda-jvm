package pl.sda.jvm;

import java.util.LinkedList;
import java.util.List;

public class DwaObiekyWHeap {

    public static void main(String[] args) throws InterruptedException {
        List<MojObiekt> mojObiekts = new LinkedList<>();
        new Thread(() -> {
            while (true) {
                mojObiekts.add(new MojObiekt());
                MojObiektDwa.pole += 1;
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException e) {
                }
                System.out.println(MojObiektDwa.pole);
            }
        }).start();

    }
}

class MojObiektDwa {
    public static int pole = 0;
}

class MojObiekt {
    private int pole;

    public int getPole() {
        return pole;
    }

    public void setPole(int pole) {
        this.pole = pole;
    }
}