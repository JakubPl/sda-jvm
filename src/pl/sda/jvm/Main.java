package pl.sda.jvm;

import java.util.LinkedList;

public class Main {
    public static void main(String[] args) {
        final LinkedList<String> lista = new LinkedList<>();

        final Thread thread2 = new Thread(() -> {
            while (true) {
                int a = 1;
                lista.add("cde");
            }
        }
        );
        final Thread thread1 = new Thread(() -> {
            while (true) {
                int i = 2;
                lista.add("abc");
            }
        }
        );

        thread1.start();
        thread2.start();
    }
}